# GitHub Browser

[![Netlify Status](https://api.netlify.com/api/v1/badges/df30a2d1-bb5f-4175-893a-e3046e72dbe2/deploy-status)](https://app.netlify.com/sites/github-browser/deploys)

A SPA to browse repositories belonging to any organization on GitHub.

[Live deployment](https://github-browser.netlify.com/)

## Hacking

Prerequisites:

  - Node.js 
  - NPM >= 6.5.0 (for the `clean-install` command)
  - GNU Make

With the above, clone the repository and from within it's directory call: 

```sh
make run
```

It should install all dependencies and start a local development server. The address will be displayed (something like `Server now ready on http://localhost:54321`, but the port may be different).

### Running test

```
make tests
```

The tests are also executed in CI ([GitLab](https://gitlab.com/tad-lispy/github-browser/pipelines)) and CD ([Netlify](https://app.netlify.com/sites/github-browser/deploys)).

## Features

  - Client side navigation (push state)
  - Elm + vanilla Bootstrap
  - GitHub REST API (v3)
  - GitLab CI setup
  - Hot reloading
  - Integration tests with Cypress 
  - Netlify integration

## Tech stack

Here are my choices with some explanation

### Elm

A delightful, purely functional language for web applications. 

  - type safty
  - fun to write
  - easy maintainable
  - the best community I have ever been part of

### Bootstrap

I would use [Elm UI](https://package.elm-lang.org/packages/mdgriffith/elm-ui/latest/) but I didn't want to overwhelm the reader with too much novelty.

### Netlify

It's quite easy to deploy a website like this with automatic TLS, own free subdomain etc. 

### Cypress

Considering limited time I decided to write only integration tests. With the safety provided by Elm's compiler and relatively simple business logic it seemed like the best time investment.

### Parcel

A zero-configuration JS + assets bundler and developemnt server. It just works. 

### Make

I'm still [making my mind if it's a good idea](https://stackoverflow.com/questions/55278311/is-it-a-good-idea-to-call-make-from-npm-scripts) but there are definitely some compelling arguments:

  - it's very maure (40 years in production)
  - it's very stable (esp. compared to JS ecosystem in which there seems to be a new tool every week)
  - it's a free software backed by GNU

So far I'm happy with it.

