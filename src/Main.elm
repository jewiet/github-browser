module Main exposing (main)

import Browser exposing (Document, UrlRequest)
import Browser.Navigation as Navigation
import Dict exposing (Dict)
import Html exposing (Html)
import Html.Attributes
import Html.Events
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode
import List.Extra as List
import Maybe.Extra as Maybe
import Paginated
import Url exposing (Url)
import Url.Parser exposing ((</>))


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , onUrlChange = UrlChanged
        , onUrlRequest = UrlRequested
        , update = update
        , subscriptions = subscriptions
        , view = view
        }


type alias Model =
    { navigationKey : Navigation.Key
    , route : Route
    , repos : WebData Repos
    , branches : WebData Branches
    , order : Order
    , orgInput : String
    , languageFilter : Maybe String
    }


type Route
    = StartRoute
    | OrgRoute String
    | RepoRoute String String
    | UndefinedRoute


type alias Order =
    { metric : Metric
    , direction : Direction
    }


type Direction
    = Descending
    | Ascending


type Metric
    = Name
    | Forks
    | Stars


type WebData data
    = NotAsked
    | Loading
    | Success data
    | Failure Http.Error


type alias Repos =
    List Repo


type alias Repo =
    { name : String
    , description : Maybe String
    , language : Maybe String
    , stars : Int
    , forks : Int
    }


type alias Branches =
    List Branch


type alias Branch =
    { name : String
    }


type alias Flags =
    Encode.Value


init : Flags -> Url -> Navigation.Key -> ( Model, Cmd Msg )
init flags_ url navigationKey =
    let
        route =
            parseUrl url
    in
    ( { navigationKey = navigationKey
      , route = route
      , repos = NotAsked
      , branches = NotAsked
      , orgInput = ""
      , order = Order Stars Descending
      , languageFilter = Nothing
      }
    , routeCmd route
    )


parseUrl : Url -> Route
parseUrl url =
    url
        |> Url.Parser.parse urlParser
        |> Maybe.withDefault UndefinedRoute


urlParser =
    Url.Parser.oneOf
        [ Url.Parser.map RepoRoute (Url.Parser.string </> Url.Parser.string)
        , Url.Parser.map OrgRoute Url.Parser.string
        , Url.Parser.map StartRoute Url.Parser.top
        ]


getRepos : String -> Cmd Msg
getRepos orgName =
    let
        url =
            "https://api.github.com/orgs/"
                ++ orgName
                ++ "/repos"

        resultToMsg : Result Http.Error Repos -> Msg
        resultToMsg result =
            ReposResponse <|
                case result of
                    Ok data ->
                        Success data

                    Err error ->
                        Failure error
    in
    Paginated.get url repoDecoder
        |> Paginated.send resultToMsg


repoDecoder : Decoder Repo
repoDecoder =
    Decode.map5 Repo
        (Decode.field "full_name" Decode.string)
        (Decode.field "description" (Decode.maybe Decode.string))
        (Decode.field "language" (Decode.maybe Decode.string))
        (Decode.field "stargazers_count" Decode.int)
        (Decode.field "forks_count" Decode.int)


getBranches : String -> String -> Cmd Msg
getBranches orgName repoName =
    let
        url =
            "https://api.github.com/repos/"
                ++ orgName
                ++ "/"
                ++ repoName
                ++ "/branches"

        resultToMsg : Result Http.Error Branches -> Msg
        resultToMsg result =
            BranchesResponse <|
                case result of
                    Ok data ->
                        Success data

                    Err error ->
                        Failure error
    in
    Paginated.get url branchDecoder
        |> Paginated.send resultToMsg


branchDecoder : Decoder Branch
branchDecoder =
    Decode.map Branch
        (Decode.field "name" Decode.string)


type Msg
    = UrlRequested UrlRequest
    | UrlChanged Url
    | ReposResponse (WebData Repos)
    | BranchesResponse (WebData Branches)
    | OrgInput String
    | FindOrg
    | ToggleOrder Metric
    | ToggleLanguageFilter String


routeCmd : Route -> Cmd Msg
routeCmd route =
    case route of
        StartRoute ->
            Cmd.none

        OrgRoute orgName ->
            getRepos orgName

        RepoRoute orgName repoName ->
            getBranches orgName repoName

        UndefinedRoute ->
            Cmd.none


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UrlRequested (Browser.Internal url) ->
            ( model
            , url
                |> Url.toString
                |> Navigation.pushUrl model.navigationKey
            )

        UrlRequested (Browser.External url) ->
            ( model
            , Navigation.load url
            )

        UrlChanged url ->
            let
                route =
                    parseUrl url
            in
            ( { model
                | route = route
                , repos = Loading
                , branches = Loading
              }
            , routeCmd route
            )

        ReposResponse data ->
            ( { model | repos = data }
            , Cmd.none
            )

        BranchesResponse data ->
            ( { model | branches = data }
            , Cmd.none
            )

        OrgInput input ->
            ( { model | orgInput = input }
            , Cmd.none
            )

        FindOrg ->
            ( { model | repos = Loading }
            , model.orgInput
                |> (++) "/"
                |> Navigation.pushUrl model.navigationKey
            )

        ToggleOrder metric ->
            let
                newOrder =
                    if model.order.metric == metric then
                        case model.order.direction of
                            Ascending ->
                                Order metric Descending

                            Descending ->
                                Order metric Ascending

                    else
                        Order metric Ascending
            in
            ( { model | order = newOrder }
            , Cmd.none
            )

        ToggleLanguageFilter language ->
            let
                newLanguageFilter =
                    if model.languageFilter == Just language then
                        Nothing

                    else
                        Just language
            in
            ( { model | languageFilter = newLanguageFilter }
            , Cmd.none
            )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


view : Model -> Document Msg
view model =
    case model.route of
        StartRoute ->
            startView model

        OrgRoute orgName ->
            orgView orgName model

        RepoRoute orgName repoName ->
            repoView orgName repoName model

        UndefinedRoute ->
            undefinedView model


startView model =
    page ""
        [ findOrgElement model.orgInput ]


orgView orgName model =
    case model.repos of
        NotAsked ->
            page "Initializing" [ Html.text "Initializing..." ]

        Loading ->
            page "Loading" [ Html.text "Loading repos..." ]

        Failure (Http.BadUrl description) ->
            page "Error"
                [ errorElement "The name of the org seems incorrect" model.orgInput ]

        Failure (Http.BadPayload description body) ->
            page "Error"
                [ errorElement "There was an error. Sorry." model.orgInput ]

        Failure Http.Timeout ->
            page "Error"
                [ errorElement "There was no response. Sorry." model.orgInput ]

        Failure Http.NetworkError ->
            page "Error"
                [ errorElement "There was an error. Sorry. Are you connected to the Internet?" model.orgInput ]

        Failure (Http.BadStatus response) ->
            if response.status.code == 404 then
                page "Not Found"
                    [ errorElement "Org not found. Sorry. Please try again."
                        model.orgInput
                    ]

            else
                page "Error"
                    [ errorElement "There was an error. Sorry." model.orgInput ]

        Success repos ->
            reposElements repos model
                |> page orgName


repoView orgName repoName model =
    case model.branches of
        NotAsked ->
            page "Initializing" [ Html.text "Initializing..." ]

        Loading ->
            page "Loading" [ Html.text "Loading branches..." ]

        Failure (Http.BadUrl description) ->
            page "Error"
                [ errorElement "The name of the repo seems incorrect" model.orgInput ]

        Failure (Http.BadPayload description body) ->
            page "Error"
                [ errorElement "There was an error. Sorry." model.orgInput ]

        Failure Http.Timeout ->
            page "Error"
                [ errorElement "There was no response. Sorry." model.orgInput ]

        Failure Http.NetworkError ->
            page "Error"
                [ errorElement "There was an error. Sorry. Are you connected to the Internet?" model.orgInput ]

        Failure (Http.BadStatus response) ->
            if response.status.code == 404 then
                page "Not Found"
                    [ errorElement "Repo not found. Sorry. Please try again." model.orgInput ]

            else
                page "Error"
                    [ errorElement "There was an error. Sorry." model.orgInput ]

        Success branches ->
            page (orgName ++ " / " ++ repoName ++ " branches")
                [ branchesElement branches
                ]


undefinedView model =
    page "Not Found"
        [ errorElement "The address seems incorrect. Where did you get that link from?" model.orgInput ]


page : String -> List (Html Msg) -> Document Msg
page title elements =
    { title =
        if String.isEmpty title then
            "GitHub Browser"

        else
            title ++ " | GitHub Browser"
    , body =
        [ Html.nav [ Html.Attributes.class "navbar navbar-expand-md navbar-dark bg-dark" ]
            [ Html.a
                [ Html.Attributes.href "/"
                , Html.Attributes.class "navbar-brand"
                ]
                [ Html.text "GitHub Browser" ]
            , Html.div
                [ Html.Attributes.class "collapse navbar-collapse" ]
                [ Html.ul
                    [ Html.Attributes.class "navbar-nav mr-auto" ]
                    [ Html.li
                        [ Html.Attributes.class "nav-item text-white-50"
                        ]
                        [ Html.text title ]
                    ]
                ]
            ]
        , Html.main_ [ Html.Attributes.class "container" ] elements
        ]
    }


errorElement : String -> String -> Html Msg
errorElement message input =
    Html.div
        [ Html.Attributes.class "row" ]
        [ Html.div
            [ Html.Attributes.class "col-lg-12 mt-5" ]
            [ Html.div
                [ Html.Attributes.class "alert alert-warning" ]
                [ Html.text message ]
            , findOrgElement input
            ]
        ]


reposElements : Repos -> Model -> List (Html Msg)
reposElements repos model =
    let
        list =
            repos
                |> List.filter
                    (\repo ->
                        case model.languageFilter of
                            Nothing ->
                                True

                            Just language ->
                                repo.language == Just language
                    )
                |> sort model.order
                |> List.map repoElement
                |> Html.div
                    [ Html.Attributes.class "row"
                    ]

        controls =
            [ sortButtons
            , languageFilter
            ]
                |> Html.div
                    [ Html.Attributes.class "row mt-3 mb-3"
                    ]

        sortButtons =
            [ Html.h6 [] [ Html.text "sort by" ]
            , Html.div [ Html.Attributes.class "btn-group btn-group-sm" ]
                [ Html.button
                    [ Html.Events.onClick (ToggleOrder Name)
                    , Html.Attributes.class "btn btn-outline-secondary"
                    ]
                    [ Html.text "Name" ]
                , Html.button
                    [ Html.Events.onClick (ToggleOrder Stars)
                    , Html.Attributes.class "btn btn-outline-secondary"
                    ]
                    [ Html.text "Stars" ]
                , Html.button
                    [ Html.Events.onClick (ToggleOrder Forks)
                    , Html.Attributes.class "btn btn-outline-secondary"
                    ]
                    [ Html.text "Forks" ]
                ]
            ]
                |> Html.div [ Html.Attributes.class "col-lg-3" ]

        languageFilter : Html Msg
        languageFilter =
            [ Html.h6 [] [ Html.text "filter language" ]
            , repos
                |> List.map .language
                |> Maybe.values
                |> List.sort
                |> List.unique
                |> List.map
                    (\language ->
                        languageCheckbox
                            language
                            (model.languageFilter == Just language)
                    )
                |> Html.div []
            ]
                |> Html.div [ Html.Attributes.class "col-lg-9" ]
    in
    [ controls
    , list
    , controls
    ]


branchesElement : Branches -> Html Msg
branchesElement branches =
    branches
        |> List.map branchElement
        |> Html.ul [ Html.Attributes.class "list-group list-group-flush" ]


branchElement : Branch -> Html Msg
branchElement branch =
    branch.name
        |> Html.text
        |> List.singleton
        |> Html.li [ Html.Attributes.class "list-group-item" ]


languageCheckbox : String -> Bool -> Html Msg
languageCheckbox language checked =
    [ Html.input
        [ Html.Attributes.id language
        , Html.Attributes.type_ "checkbox"
        , Html.Events.onClick (ToggleLanguageFilter language)
        , Html.Attributes.checked checked
        , Html.Attributes.class "form-check-input"
        ]
        []
    , Html.label
        [ Html.Attributes.for language
        , Html.Attributes.class "form-check-label"
        ]
        [ Html.text language ]
    ]
        |> Html.div [ Html.Attributes.class "form-check form-check-inline" ]


sort order list =
    let
        ascending =
            case order.metric of
                Name ->
                    List.sortBy .name list

                Forks ->
                    List.sortBy .forks list

                Stars ->
                    List.sortBy .stars list
    in
    case order.direction of
        Ascending ->
            ascending

        Descending ->
            List.reverse ascending


repoElement : Repo -> Html Msg
repoElement repo =
    Html.div [ Html.Attributes.class "col-lg-12 mt-3" ]
        [ Html.div [ Html.Attributes.class "card" ]
            [ Html.div [ Html.Attributes.class "card-body" ]
                [ Html.h5
                    [ Html.Attributes.class "card-title" ]
                    [ Html.text repo.name ]
                , Html.p []
                    [ repo.description
                        |> Maybe.withDefault ""
                        |> Html.text
                    ]
                , Html.div [ Html.Attributes.class "d-flex justify-content-between align-items-center" ]
                    [ Html.a
                        [ Html.Attributes.href ("/" ++ repo.name)
                        , Html.Attributes.class "btn btn-primary btn-sm stretched-link"
                        ]
                        [ Html.text "Branches" ]
                    , Html.div []
                        [ Html.span
                            [ Html.Attributes.class "badge badge-secondary" ]
                            [ repo.stars
                                |> String.fromInt
                                |> Html.text
                            , Html.text " stars"
                            ]
                        , Html.text " "
                        , Html.span
                            [ Html.Attributes.class "badge badge-secondary" ]
                            [ repo.forks
                                |> String.fromInt
                                |> Html.text
                            , Html.text " forks"
                            ]
                        ]
                    ]
                ]
            ]
        ]


findOrgElement : String -> Html Msg
findOrgElement input =
    Html.form [ Html.Events.onSubmit FindOrg ]
        [ Html.div [ Html.Attributes.class "input-group input-group-lg mt-5" ]
            [ Html.input
                [ Html.Events.onInput OrgInput
                , Html.Attributes.value input
                , Html.Attributes.class "form-control"
                , Html.Attributes.placeholder "Enter a name of a GitHub organisation to start"
                ]
                []
            , Html.div [ Html.Attributes.class "input-group-append" ]
                [ Html.button
                    [ Html.Attributes.type_ "submit"
                    , Html.Attributes.class "btn btn-primary"
                    ]
                    [ Html.text "Find"
                    ]
                ]
            ]
        ]
